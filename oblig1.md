# INF222 V24 - Obligatory Assignment 1 (deadline: 15.2.2024 at 23:59)

# Task

Below is an informal description of the constructs of an imaginary programming language.

Your task is to:
- [ ] **Specify an abstract syntax of the language in Haskell.**
- [ ] **Specify an abstract syntax of the language in Zephyr ASDL.**
- [ ] **Specify a concrete syntax of the language in a variant of Extended Backus-Naur Form, i.e., to specify a grammar for this language using a notation that we used during the lectures. You do not need to worry about ambiguity of your grammar. Please see Section "Ideas for concrete syntax" below in this document.** 
- [ ] **Write a short (20-30) lines sample program using the concrete syntax. Try to demonstrate as many of language constructs as possible in that sample.**

- [ ] **Reflect upon the suggested language "design", and identify at least three problems.**
	* _Hint 1: something about functions._
	* _Hint 2: something about arithmetic expressions._
	* _Hint 3: something about classes._

- [ ] **Do you see any problems regarding the orthogonality of this language's constructs?**


# Language Constructs

* variable declaration
	- untyped
	- a variable declaration may optionally also specify an initial value (concrete syntax example: `var x = 10;`)
		- the initial value can only be an integer/boolean/string literal
* array declaration
	- untyped
	- one-dimensional
	- without an initial value
* `if`/`then`/`else`/`elseif` statement
	- `else` branch can be omitted
		- if omitted, there can be several `elseif`-branches
* `switch`/`case`/`default` statement
	- at least one `case`
	- the `default`-branch can be omitted
	- at most one `default`-branch
* `while`/`do` statement
	- inside a body of a loop, the following loop-specific statements should be supported:
		- `stop`/`if` statement: to exit from an iteration if a condition is met (sample in concrete syntax: `stop if (a+b==c)`)
		- `stop` statement: similar to `stop`/`if`, but without a condition
		- `next` statement: to skip several iterations of a loop (samples in concrete syntax: `next 10` -- skips 10 iterations of a loop, `next (2+3)` -- skips 5 iterations of a loop, etc.)
* block statement
* function declaration
	- untyped, i.e., the type of a value returned by a function is not specified
	- several parameters can be specified
		- but at least one parameter should always be specified
		- parameters are untyped
		- for each parameter, it should be possible to specify whether:
			- it is optional or not
			- its default value 
	- sample in conrete syntax: `func f(a, b, optional c, d, optional e, f, x=10, y, z) { /*function body*/ }`
* arithmetic expressions
	* primitive expressions:
		* some are taken for granted (i.e., no need to specify): integer literals, boolean literals, string literals
		* using a variable
		* accessing a class member
		* indexing an array element
		* function call
		* (potentially, other primitive expressions may be needed)
	* binary operators: addition, subtraction, multiplication, equality, logical AND, logical OR
	* unary operators: "tilde" (`~`)
		- Note: you only have to specify the syntax of language, not the semantics. Thus, what this operator is supposed to do, is not relevant for Oblig1. However, for the completeness of the explanation, this operator:
			- for integers, would mean negation (e.g., `~10` means negative 10)
			- for booleans, would mean logical NOT (e.g., `~true` means `false`)
			- for strings, would mean reverse (e.g., `~"abc"` means `"cba"`)
* `class` declarations
	* the following member declarations should be supported:
		* fields
			* untyped
		* methods
			* no return type
			* syntax is the same as function declarations
		* properties
			* similar to fields, but also supports specifying a getter and a setter
				* the user may omit specifying the getter / the setter / both
			* can be specified as read-only
			* can be specified as write-only
			* can either be read-only _or_ write-only _or_ ordinary (i.e., not explicitly specified as read-only nor write-only)
			* sample in concrete syntax: `class C { readonly property P getter { /*some code*/ } setter {/*some code*/}; writeonly property H; property J; }`
	* can extend several other classes (sample in concrete syntax: `class A extends B, C { var id; }`)
* If needed for completeness of the abstract/concrete syntax, more constructs.


# Ideas for concrete syntax

You can be rather creative here. To start off of some ideas, please select one of the following options based on the last digit of your UiB user handle (usually, it's three letters followed by three/more digits -- so, select the very last digit).

| digit | syntax |
|---|---|
| 0 | Java-like (curly braces) |
| 1 | Pascal-like (`begin` ... `end`) |
| 2 | Visual Basic-like (`if` ... `end if`, `while` ... `end while`, `function` ... `end function`, `class` ... `end class`) |
| 3 | Java-like (curly braces) |
| 4 | Pascal-like (`begin` ... `end`) |
| 5 | Visual Basic-like (`if` ... `end if`, `while` ... `end while`, `function` ... `end function`, `class` ... `end class`) |
| 6 | Java-like (curly braces) |
| 7 | Pascal-like (`begin` ... `end`) |
| 8 | Visual Basic-like (`if` ... `end if`, `while` ... `end while`, `function` ... `end function`, `class` ... `end class`) |
| 9 | Java-like (curly braces) |
	
Please also use one of the following concrete syntax ideas, again based on the last digit of your UiB user handle.

| digit | idea | example
|---|---|---|
| 0 | English-like syntax for function calls | e.g., `call f of x and y and z` instead of `f(x,y,z)` |
| 1 | English-like syntax for accessing class members | e.g., `name of person1` instead of `person1.name` |
| 2 | English-like syntax for indexing array elements | e.g., `myArray at i+1` instead of `myArray[i+1]` |
| 3 | English-like syntax for assignment statements | e.g., `set 1+2+3 to x` instead of `x = 1+2+3` |
| 4 | English-like syntax for arithmetic expressions|  e.g., `2 plus 3 mult 5` instead of `2+3*5` |
| 5 | English-like syntax for function calls | e.g., `call f of x and y and z` instead of `f(x,y,z)` |
| 6 | English-like syntax for accessing class members | e.g., `name of person1` instead of `person1.name` |
| 7 | English-like syntax for indexing array elements | e.g., `myArray at i+1` instead of `myArray[i+1]` |
| 8 | English-like syntax for assignment statements | e.g., `set 1+2+3 to x` instead of `x = 1+2+3` |
| 9 | English-like syntax for arithmetic expressions|  e.g., `2 plus 3 mult 5` instead of `2+3*5` |
